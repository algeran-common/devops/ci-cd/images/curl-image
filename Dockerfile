FROM    alpine:3.10

ARG     CURL_VERSION
ARG     JQ_VERSION

RUN     apk update \
          && apk add \
              curl=${CURL_VERSION} \
              jq=${JQ_VERSION}
